package main;

public class DataValidation extends Middleware { //проверка данных
    private Parcel parcel;

    public DataValidation(Parcel aPackage) {
        this.parcel = aPackage;
    }

    public Parcel getParcel() {
        return parcel;
    }

    public void setParcel(Parcel parcel) {
        this.parcel = parcel;
    }

    @Override
    public boolean check(String fio, String address) {
        if (!parcel.getFio().equals(fio)) {
            System.out.println("ФИО не совпадает");
            return false;
        }
        if (!parcel.getAddress().equals(address)) {
            System.out.println("Адрес не совпадает");
            return false;
        }
        System.out.println("Можете получить посылку");
        return checkNext(fio,address);
    }
}
