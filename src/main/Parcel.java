package main;

public class Parcel { //посылка
    private String fio;
    private String address;
    private int weight;

    public Parcel(String fio, String address, int weight) {
        this.fio = fio;
        this.address = address;
        this.weight = weight;
    }

    public String getFio() {
        return fio;
    }

    public void setFio(String fio) {
        this.fio = fio;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }
}
