package main;

public class CheckingDelivery extends Middleware { //доставили посылку или нет
    private Parcel parcel;
    private boolean delivery;

    public CheckingDelivery(Parcel aPackage, boolean delivery) {
        this.parcel = aPackage;
        this.delivery = delivery;
    }

    public Parcel getParcel() {
        return parcel;
    }

    public void setParcel(Parcel parcel) {
        this.parcel = parcel;
    }

    public boolean isDelivery() {
        return delivery;
    }

    public void setDelivery(boolean delivery) {
        this.delivery = delivery;
    }

    @Override
    public boolean check(String fio, String address) {
        if (!this.delivery) {
            System.out.println("Посылка не доставлена");
            return false;
        } else
            return checkNext(fio, address);
    }
}
