package main;

public abstract class Middleware {
    private Middleware next;

    public Middleware linkWith(Middleware next) {
        this.next = next;
        return next;
    }

    public abstract boolean check(String fio, String address);

    protected boolean checkNext(String fio, String address) {
        if (next == null) {
            return true;
        }
        return next.check(fio, address);
    }

}
