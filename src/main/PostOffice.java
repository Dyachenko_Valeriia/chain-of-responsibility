package main;

public class PostOffice {
    private Middleware middleware;

    public void setMiddleware(Middleware middleware) {
        this.middleware = middleware;
    }

    public boolean logIn(String fio, String address) {
        if (middleware.check(fio, address)) {
            System.out.println("Вы получили посылку");
            return true;
        }
        return false;
    }
}
