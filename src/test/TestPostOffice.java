package test;

import main.*;
import org.junit.Test;

import static org.junit.Assert.*;

public class TestPostOffice {
    @Test
    public void testPostOffice(){
        PostOffice postOffice = new PostOffice();
        Parcel parcel = new Parcel("Дьяченко Валерия Витальевна", "Сибирская 7",20);
        Middleware middleware = new CheckingDelivery(parcel,true);
        middleware.linkWith(new DataValidation(parcel));
        postOffice.setMiddleware(middleware);
        assertTrue(postOffice.logIn("Дьяченко Валерия Витальевна", "Сибирская 7"));
    }

    @Test
    public void testPostOffice1(){
        PostOffice postOffice = new PostOffice();
        Parcel parcel = new Parcel("Дьяченко Валерия Витальевна", "Сибирская 7",20);
        Middleware middleware = new CheckingDelivery(parcel,true);
        middleware.linkWith(new DataValidation(parcel));
        postOffice.setMiddleware(middleware);
        assertFalse(postOffice.logIn("Цалко Виктория Алексеевна", "Сибирская 7"));
    }

    @Test
    public void testPostOffice2(){
        PostOffice postOffice = new PostOffice();
        Parcel parcel = new Parcel("Дьяченко Валерия Витальевна", "Сибирская 7",20);
        Middleware middleware = new CheckingDelivery(parcel,true);
        middleware.linkWith(new DataValidation(parcel));
        postOffice.setMiddleware(middleware);
        assertFalse(postOffice.logIn("Дьяченко Валерия Витальевна", "Сибирская 9"));
    }

    @Test
    public void testPostOffice3(){
        PostOffice postOffice = new PostOffice();
        Parcel parcel = new Parcel("Дьяченко Валерия Витальевна", "Сибирская 7",20);
        Middleware middleware = new CheckingDelivery(parcel,false);
        middleware.linkWith(new DataValidation(parcel));
        postOffice.setMiddleware(middleware);
        assertFalse(postOffice.logIn("Дьяченко Валерия Витальевна", "Сибирская 7"));
    }
}
